//
//  appWindow_UIWindow.swift
//  collectionViewWithSearchBar
//
//  Created by wahyu.s.j.saputra on 03/06/20.
//  Copyright © 2020 wahyu.s.j.saputra. All rights reserved.
//

import UIKit

class appWindow_UIWindow: UIWindow {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override var windowLevel: UIWindow.Level {
        get {
            return UIWindow.Level(rawValue: CGFloat.greatestFiniteMagnitude - 1)
        }
        set { }
    }

}
