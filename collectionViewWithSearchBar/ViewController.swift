//
//  ViewController.swift
//  collectionViewWithSearchBar
//
//  Created by wahyu.s.j.saputra on 03/06/20.
//  Copyright © 2020 wahyu.s.j.saputra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    public var message : String!
    @IBOutlet weak var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        preferredContentSize = CGSize(width: 200, height: 100)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textLabel.text = message
    }


}

