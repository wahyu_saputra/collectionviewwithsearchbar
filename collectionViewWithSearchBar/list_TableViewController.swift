//
//  list_TableViewController.swift
//  collectionViewWithSearchBar
//
//  Created by wahyu.s.j.saputra on 03/06/20.
//  Copyright © 2020 wahyu.s.j.saputra. All rights reserved.
//

import UIKit

class list_TableViewController: UITableViewController, UISearchResultsUpdating , UIPopoverPresentationControllerDelegate{

    var segmentedController: UISegmentedControl!
    var searchController: UISearchController!
    var buttonLeft:UIBarButtonItem!
    
    @IBOutlet weak var buttonRight: UIBarButtonItem!
    
    var dimmerView :UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        //searching in navbar
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        //searchController.searchBar.placeholder = "Search artists"
        self.navigationItem.searchController = searchController
        //self.navigationItem.hidesSearchBarWhenScrolling = false
        self.definesPresentationContext = true
        
        //searching
        let items = ["Vowel", "Consonant"]
        segmentedController = UISegmentedControl(items: items)
        segmentedController.selectedSegmentIndex = 0
        segmentedController.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)
        navigationItem.titleView = segmentedController
        
        //MARK: CUSTOM VIEW
//        let customView = UIView()
//        customView.translatesAutoresizingMaskIntoConstraints = false
//        customView.addConstraint(NSLayoutConstraint(item: customView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40.0))
//        customView.addConstraint(NSLayoutConstraint(item: customView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40.0))
////        customView.backgroundColor = UIColor(white: 0.5, alpha: 1.0)
//        let btn = UIButton(frame: customView.frame)
//        customView.addSubview(btn)
//
////        btn.layer.backgroundColor = UIColor.red.cgColor
////        btn.layer.borderWidth = 2.0
////        btn.layer.borderColor = UIColor.black.cgColor
////
//        btn.setTitle("Test Button", for: .normal)
//        btn.titleColor(for: .normal)
////        btn.addTarget(self, action: #selector(segmentChanged(_:)), for: .touchUpInside)
////        btn.backgroundColor = UIColor.red
////        btn.leadingAnchor.constraint(equalTo: customView.leadingAnchor, constant: 0).isActive = true
////        btn.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0).isActive = true
////        btn.trailingAnchor.constraint(equalTo: customView.trailingAnchor, constant: 0).isActive = true
////        btn.bottomAnchor.constraint(equalTo: customView.bottomAnchor, constant: 0).isActive = true
//        btn.sizeToFit()
//
//        buttonLeft = UIBarButtonItem(customView: customView)
//        navigationItem.leftBarButtonItem = buttonLeft
        //MARK: CUSTOM VIEW END
        
        let buttonMenu = UIButton(type: .system)
        buttonMenu.setTitle(buttonRight.title, for: .normal)
        buttonMenu.setTitleColor(buttonRight.tintColor, for: .normal)
        buttonMenu.backgroundColor = .systemBackground
        buttonMenu.sizeToFit()
        buttonMenu.addTarget(self, action: #selector(menuTouch(_:)), for: .touchUpInside)
        buttonRight.customView = buttonMenu

        ///let newButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(handleSignOut))
        //navigationItem.rightBarButtonItem?.tintColor = UIColor.black
    }
    
    
    @IBAction func menuTouch(_ sender: Any) {
        print("clicked")
    }
    
    
    @IBAction func closeDimm(_ sender: Any) {
        print("Start remove subView")
        dimmerView.removeFromSuperview()
    }
    
    
    override func viewDidLayoutSubviews() {
        //let image = view.snapshot()
        
        //return
        //MARK: focused control feature (must UIView)
        let focusedFeature = buttonRight.value(forKey: "view") as! UIView
        
        
        //MARK: option 1 rendering image view
//        let renderer = UIGraphicsImageRenderer(bounds: focusedFeature.bounds)
//        let imageView = UIImageView(image: renderer.image(actions: { rendererContext in
//            focusedFeature.layer.render(in: rendererContext.cgContext)
//        }))
        
        //MARK: option 2 if object is UIView then just 'snapshot'
        focusedFeature.layer.cornerRadius = 8 //focusedFeature.frame.width/2
        guard let snapshot = focusedFeature.snapshotView(afterScreenUpdates: false) else{
            return
        }
        focusedFeature.layer.cornerRadius = 0
        
        //MARK:adding to window
        let window = UIApplication.shared.windows[0]
        dimmerView = UIView(frame: window.bounds)

        window.addSubview(dimmerView);
        dimmerView.addSubview(snapshot)
        
        //MARK:let background is black and 0.5 alpha
        let backgroundDuration: TimeInterval = 0.1
        UIView.animate(withDuration: backgroundDuration) {
            self.dimmerView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeDimm(_:)))
        dimmerView.addGestureRecognizer(tap)
        
        //MARK:position to the right place
        guard let focusedViewSuperview = focusedFeature.superview else {
            return
        }
        let convertedFrame = dimmerView.convert(focusedFeature.frame, from: focusedViewSuperview)
        
        //MARK:Setposition of snap
        snapshot.frame = convertedFrame
//        imageView.frame = convertedFrame
        
        //MARK:show the cloud label
//        let vc = ViewController()
//        vc.modalPresentationStyle = .popover
//        vc.popoverPresentationController?.delegate = self
//        vc.popoverPresentationController?.sourceView = focusedFeature
//        vc.popoverPresentationController?.sourceRect = focusedFeature.bounds
//        vc.popoverPresentationController?.backgroundColor = UIColor.white.withAlphaComponent(0.9)
//
//        present(vc, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "popUpMenu") as! ViewController
        vc.modalPresentationStyle = .popover
        vc.popoverPresentationController?.delegate = self
        vc.popoverPresentationController?.sourceView = focusedFeature
        vc.popoverPresentationController?.sourceRect = focusedFeature.bounds
        //let popover: UIPopoverPresentationController = vc.popoverPresentationController!
        //popover.barButtonItem = sender
        vc.message = "Access the Korean alphabets lists"
        present(vc, animated: true, completion:nil)
    }
    
    @objc func segmentChanged(_ sender: UISegmentedControl) {
        print("Segmented Change")
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = "HELLO WORLD \(indexPath.row)"

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func updateSearchResults(for searchController: UISearchController) {
        print("search")
    }
    
    

}
